let token=localStorage.getItem("token"); 

let profileContainer=document.querySelector("#profileContainer");


let adminUser=localStorage.getItem("isAdmin");  
let modalButton=document.querySelector('#editProfileButton');
let userId=localStorage.getItem("id"); 


if(adminUser=="false"){
	modalButton.innerHTML=
		`
		<div class="col-md-2 offset-md-5 mt-5">
			<a href="./editProfile.html?userId=${userId}" value="${userId}" class="btn btn-block btn-primary">
				Edit Profile
			</a>
		</div>
		`
}else{
	modalButton.innerHTML=null
}


if(!token||token==null){
	alert ('You must login first.');
	window.location.replace("./login.html");
}else{
	fetch('https://evening-ridge-93310.herokuapp.com/api/users/details/',{
		headers:{
			'Authorization':`Bearer ${token}`
		}
	})
	.then(res=>res.json())
	.then(data=>{
		console.log(data);

		profileContainer.innerHTML=
		`
		<div class="col-md-12">
			<section class="jumbotron my-5">
				<h4 class="text-center">Name: ${data.firstName} ${data.lastName}</h4>
				<h4 class="text-center">Email: ${data.email}</h4>
				<h4 class="text-center">Mobile Number:${data.mobileNo}</h4>
				<h4 class="text-center mt-5">Class History</h4>
				<table class="table">
					<thead>
						<tr>
							<th>Course Name</th>
							<th>Enrolled On</th>
							<th>Status</th>	
						</tr>
					</thead>
					<tbody id="courses">
					</tbody>
				</table>


			</section>
		</div>
		`

		let courses=document.querySelector("#courses");

		data.enrollments.forEach(courseData=>{

			fetch(`https://evening-ridge-93310.herokuapp.com/api/courses/${courseData.courseId}`)
			.then(res=>res.json())
			.then(data=>{
				courses.innerHTML+=
				`
				<tr>
					<td>${data.name}</td>
					<td>${courseData.enrolledOn.substring(0,10)}</td>
					<td>${courseData.status}</td>
				</tr>
				`
			})
		})

	})
}



/*let userId=localStorage.getItem("id");
console.log(`hello`);

let token=localStorage.getItem('token');


let userName=document.querySelector('#userName');
let userEmail=document.querySelector('#userEmail');
let userMobileNo=document.querySelector('#userMobileNo');
document.querySelector("#courseHeader").innerHTML=`<h4>COURSES ENROLLED:</h4>`;

fetch(`http://localhost:4000/api/users/details/${userId}`)
.then(res=>res.json())
.then(data=>{
	console.log(data);
	userName.innerHTML=`Name: ${data.firstName} ${data.lastName}`;
	userEmail.innerHTML=`Email: ${data.email}`;
	userMobileNo.innerHTML=`Mobile No.: ${data.mobileNo}`;
	data.enrollments.map(enrollments=>{
		fetch(`http://localhost:4000/api/courses/${enrollments.courseId}`)
			.then(res=>res.json())
			.then(course=>{
				document.querySelector("#courseContainer").innerHTML+=
					`
					<div class="card">
						<h5>${course.name}</h5>
						<h6>${course.description}</h6>
						<p>Price: ₱ ${course.price}</p>
					</div>
					`	
			})		
	}).join("");
})*/