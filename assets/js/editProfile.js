// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);  

let userId = params.get('userId');
let token = localStorage.getItem('token');
let originalEmail;

fetch(`https://evening-ridge-93310.herokuapp.com/api/users/details/${userId}`)
.then(res=>res.json())
.then(data=>{
    console.log(data);
    document.querySelector("#firstName").value=data.firstName;
    document.querySelector("#firstName").placeholder=data.firstName;
    document.querySelector("#lastName").value=data.lastName;
    document.querySelector("#lastName").placeholder=data.lastName;
    document.querySelector("#mobileNumber").value=data.mobileNo;
    document.querySelector("#mobileNumber").placeholder=data.mobileNo;
    document.querySelector("#userEmail").value=data.email;
    document.querySelector("#userEmail").placeholder=data.email;
    originalEmail=data.email;
})

let updateForm=document.querySelector("#updateUser");


updateForm.addEventListener("submit",(e)=>{
    e.preventDefault(); 
    let firstName=document.querySelector("#firstName").value;
    let lastName=document.querySelector("#lastName").value;
    let mobileNumber=document.querySelector("#mobileNumber").value;
    let email=document.querySelector("#userEmail").value;
    let password1=document.querySelector("#password1").value;
    let password2=document.querySelector("#password2").value;
     
    if ((password1!==''&&password2!=='')&&(password1===password2)&&(mobileNumber.length===11)){
        fetch('https://evening-ridge-93310.herokuapp.com/api/users/email-exists',{
            method:'POST',
            headers:{
                'Content-Type':"application/json"
            },
            body:JSON.stringify({
                email:email
            })
        })
        .then(res=>res.json())
        .then(data=>{
            console.log(data)
            if (data=== false||email==originalEmail){
                fetch('https://evening-ridge-93310.herokuapp.com/api/users',{
                    method:'PUT',
                    headers:{
                        'Authorization':`Bearer ${token}`,
                        'Content-Type':"application/json"
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName:lastName,
                        email:email,
                        password:password1,
                        mobileNo:mobileNumber,
                        userId:userId
                    })
                })
                .then(res=>res.json())
                .then (data=> {
                    console.log(data);
                    if (data===true){
                        alert("Profile updated successfully!");
                        //redirect to login
                        window.location.replace("./profile.html");
                    }else{
                        alert("Something went wrong.")
                    }
                })
            }else{
                alert("Duplicate email found.")
            }
        })

    }else{
        alert("Please double check entries.")
    }
})

