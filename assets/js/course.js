//"window.location.search" returns the query string part of the URL  
console.log(window.location.search);

//Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params=new URLSearchParams(window.location.search);

//The "has" method checks if the "courseId" key exists in the URL query string
//The method returns true if the key exists
console.log(params.has('courseId'));

//The "get" method returns the value of the key passed in an argument
console.log(params.get('courseId'));

let courseId=params.get('courseId');

let token=localStorage.getItem('token');


let courseName=document.querySelector('#courseName');
let courseDesc=document.querySelector('#courseDesc');
let coursePrice=document.querySelector('#coursePrice');
let enrollContainer=document.querySelector("#enrollContainer");
let adminUser=localStorage.getItem("isAdmin");

let profileBtn=document.querySelector("#profileSession");

let userId=localStorage.getItem("id"); 
let exists;

if(adminUser=="false"){
profileBtn.innerHTML=
`
<li class="nav-item">
	<a href="./profile.html" class="nav-link"> Profile </a>
</li>
	`
}

if (adminUser=="true"){
fetch(`https://evening-ridge-93310.herokuapp.com/api/courses/${courseId}`)
.then(res=>res.json())
.then(data=>{
	console.log(data);
	courseName.innerHTML=data.name;
	courseDesc.innerHTML=data.description;
	coursePrice.innerHTML=data.price;
	let enrolleesHeader=`<h4>ENROLLEES:</h4>`;
	data.enrollees.forEach(enrollees=>{
		enrolledOn=enrollees.enrolledOn.substring(0,10);
		fetch(`https://evening-ridge-93310.herokuapp.com/api/users/details/${enrollees.userId}`)
			.then(res=>res.json())
			.then(data2=>{
				console.log(data2.firstName);
				console.log(data2.lastName);
				document.querySelector("#enrollContainer").innerHTML+=
					`
					<div class="card">
						<h5>${data2.firstName} ${data2.lastName}</h5>
						<h6>Mobile No.: ${data2.mobileNo}</h6>
						<h6>Email: ${data2.email}</h6>
						<p>Enrolled on: ${enrolledOn}</p>
					</div>
					`	
			})

			
	}).join("");
	document.querySelector("#enrollHeader").innerHTML=enrolleesHeader;
})
}else{
fetch(`https://evening-ridge-93310.herokuapp.com/api/courses/${courseId}`)
.then(res=>res.json())
.then(data=>{
	console.log(data);
	courseName.innerHTML=data.name;
	courseDesc.innerHTML=data.description;
	coursePrice.innerHTML=data.price;

	let enrollees=data.enrollees;
	enrollees.forEach(enrollees=>{
		console.log(enrollees);
		if (enrollees.userId==userId){
			exists=1
		}
	})

	if(exists>0){
		enrollContainer.innerHTML="ALREADY ENROLLED"
		exists=0
		console.log(exists)
	}else{
		enrollContainer.innerHTML=
		`
			<button id="enrollButton" class="btn btn-block btn-primary">
				Enroll
			</button>
		`
	}
	document.querySelector("#enrollButton").addEventListener("click",()=>{
		fetch('https://evening-ridge-93310.herokuapp.com/api/users/enroll',{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${token}`
			},
			body:JSON.stringify({
				courseId:courseId
			})
			})
			.then(res=>res.json())
			.then(data=>{
				console.log(data);

				if(data===true){
					alert("Thank you for enrolling! See you in class");
					window.location.replace("./courses.html");
				}else{
					alert("something went wrong");
				}
		})
	})

})
}



