let adminUser=localStorage.getItem("isAdmin");   
let cardFooter;
let modalButton=document.querySelector('#adminButton');
let profileBtn=document.querySelector("#profileSession");
let userId=localStorage.getItem("id"); 
let exists;
let enrolledOn;

if(adminUser=="false"){
profileBtn.innerHTML=
`
<li class="nav-item">
	<a href="./profile.html" class="nav-link"> Profile </a>
</li>
	`
}

if(adminUser=="false"|| !adminUser){
	modalButton.innerHTML=null;
}else{
	modalButton.innerHTML=
		`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">
				Add Course
			</a>
		</div>
		`
}


fetch('https://evening-ridge-93310.herokuapp.com/api/courses')
.then(res=>res.json())
.then(data=>{
	console.log(data);

	//creates a variable that will sore the data to be rendered
	let courseData;

	if (data.length<1){
		courseData=`No courses available`
	}else{
		courseData=data.map(course=>{// . for is for each;  .map return an array
			if (adminUser=="false"|| !adminUser){
				if(course.isActive==true){
				if(adminUser=="false"){
					let enrollees=course.enrollees;
					enrollees.forEach(enrollees=>{
						if (enrollees.userId==userId){
							exists=1
							enrolledOn=enrollees.enrolledOn.substring(0,10);
						}
					})

					if(exists>0){
						cardFooter=`ENROLLED ON ${enrolledOn}`;
						exists=0;
						enrolledOn="";
					}else{
					cardFooter=
					`
					<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block selectButton">
						Select Course
					</a>
					`
					}

				}else{
				cardFooter=
				`
				<a href="./register.html" class="btn btn-primary text-white btn-block registerButton">
					Register Now!
				</a>
				<a href="./login.html" class="btn btn-danger text-white btn-block loginButton">
					Log In
				</a>
				`
				}

				return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">
								${course.name}
							</h5>
							<p class="card-text text-left">
								${course.description}
							</p>
							<p class="card-text text-right">
								₱ ${course.price}
							</p>
						</div>
						<div class="card-footer">
						${cardFooter}
						</div>
					</div>
				</div>
				`
				)
				}
			}else{
				if(course.isActive==true){
				cardFooter=
				`
				<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block viewButton">
					View Enrollees
				</a>
				<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
					Edit
				</a>
				<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">
					Disable Course
				</a>
				`
				}else{
				cardFooter=
				`
				<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block viewButton">
					View Enrollees
				</a>
				<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
					Edit
				</a>
				<a href="./undeleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block deleteButton">
					Enable Course
				</a>
				`
				}
				return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">
								${course.name}
							</h5>
							<p class="card-text text-left">
								${course.description}
							</p>
							<p class="card-text text-right">
								₱ ${course.price}
							</p>
						</div>
						<div class="card-footer">
						${cardFooter}
						</div>
					</div>
				</div>
				`
				)
			}
			
		//replaces the commas in the array with an empty string
		}).join("");
	}

	document.querySelector("#coursesContainer").innerHTML=courseData;
})